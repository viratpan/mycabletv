<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;

use Illuminate\Database\Eloquent\SoftDeletes; //add this for SoftDeletes

class Categorie extends Model
{
    use Notifiable;
    use SoftDeletes; //add this for SoftDeletes

    protected $dates = ['deleted_at'];

    protected $fillable = [
        'name',
    ];
}
