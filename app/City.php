<?php

namespace App;
use App\State;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;

use Illuminate\Database\Eloquent\SoftDeletes; //add this for SoftDeletes

class City extends Model
{
   use Notifiable;
   use SoftDeletes; //add this for SoftDeletes

    protected $dates = ['deleted_at'];

    protected $fillable = [
        'name','states_id'
    ];

    function state(){
    	return $this->hasOne('App\State');
        //return $this->belongsTo(State::Class,'foreign_key','');
        //return $this->hasMany('App\Comment');
    }

}
