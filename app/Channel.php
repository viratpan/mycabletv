<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;

use Illuminate\Database\Eloquent\SoftDeletes; //add this for SoftDeletes

class Channel extends Model
{
    use Notifiable;
    use SoftDeletes; //add this for SoftDeletes

    protected $dates = ['deleted_at'];

    protected $fillable = [
        'channel_no','name','price','description','image','categories_id',
    ];
    public function category()
	{
	    return $this->belongsTo('App\Categorie', 'categories_id');
	}
}
