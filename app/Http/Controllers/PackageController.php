<?php

namespace App\Http\Controllers;

use App\Package;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Storage;

class PackageController extends Controller
{
    function __construct()
    {
        $this->middleware('auth:admin');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($id=null)
    {
        $tData =Package::find($id);
        $data = Package::all();
        
       
        return view('channel.package', compact('data','tData'));
    }

    

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request,$id=null)
    {
        $request->validate([
            'name' => ['required', 'string','unique:packages'],           
            'price' => ['required', 'string',],
                      
        ]);
        Package::create($request->all());
        //die($password);
        return redirect()->route('admin.package')->with('success','Package created successfully.');
    }


   
    public function update(Request $request, $id)
    {
        $lcoData = Package::find($id);
        $lcoData->name = request('name');       
        $lcoData->save();
        $request->validate([
            'name' => ['required', 'string',],            
            'price' => ['required', 'string',],
         ]);
        $lcoData->update($request->all());
       
        return redirect()->route('admin.package')
                        ->with('success',$lcoData->name.'  updated successfully');
    }

    
    public function destroy($id)
    {
         Package::find($id)->delete();   
        //$Lco = Lco::withTrashed()->get();
           
        return redirect()->route('admin.package')
                        ->with('success','Package deleted successfully');
    }

    
}
