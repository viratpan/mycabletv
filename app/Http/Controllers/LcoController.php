<?php

namespace App\Http\Controllers;

use App\Lco;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Auth;
use Illuminate\Support\Facades\Storage;
use App\Http\Controllers\Input;

class LcoController extends Controller
{
     public function __construct(Request $request)
    {
        //$this->middleware('auth');
        $this->middleware('auth:lco');
        
           
        
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        
        return view('lco.dashboard');
    }
    public function profile()
    {
        $id=Auth::guard('lco')->user()->id;
        $lcoData = Lco::find($id);
        return view('lco.profile',compact('lcoData'));
    }

    
    public function edit()
    {
         $id=Auth::guard('lco')->user()->id;
        $lcoData = Lco::find($id);
        return view('lco.edit',compact('lcoData'));
    }

    
    public function update(Request $request)
    {
        $id=Auth::guard('lco')->user()->id;
        $lcoData = Lco::find($id);
        $email=$request->email;
        unset($request['email']);//$request->request->remove('email');
        $lcoData->name = request('name');       
        $lcoData->save();

        $request->validate([
            'name' => ['required', 'string'],
            //'email' => ['required', 'string', 'email', 'max:255'],
            'mobile' => 'required',
         ]);
        /*///////////////////////////////*/
        if ($request->hasFile('registration_upload')) {
            
            if ($request->file('registration_upload')->isValid()) {  
                    $validated = $request->validate([
                        'registration_upload' => 'mimes:jpeg,png|max:1014',
                    ]);
                     $validated['name']=$request->email;
                    $extension = $request->registration_upload->extension();
                    /*/////////////////////////*/
                    $destinationPath = '/lco/'.$validated['name']; 
                            
                    if(!Storage::exists($destinationPath)) {
                        Storage::makeDirectory($destinationPath, 0775, true); //creates directory
                    } 
                /*/////////////////////////*/
                    $name="registration_upload.".$extension;
                    $request->registration_upload->storeAs($destinationPath, $name);

                $request->registration_upload=$destinationPath.$name;
                 unset($request['registration_upload']);
            }
        }
        if ($request->hasFile('postal_upload')) {
            
            if ($request->file('postal_upload')->isValid()) {  
                    $validated = $request->validate([
                        'postal_upload' => 'mimes:jpeg,png|max:1014',
                    ]);
                     $validated['name']=$request->email;
                    $extension = $request->postal_upload->extension();
                    /*/////////////////////////*/
                    $destinationPath = '/lco/'.$validated['name']; 
                            
                    if(!Storage::exists($destinationPath)) {
                        Storage::makeDirectory($destinationPath, 0775, true); //creates directory
                    } 
                /*/////////////////////////*/
                    $name="postal_upload.".$extension;
                    $request->postal_upload->storeAs($destinationPath, $name);

                $request->postal_upload=$destinationPath.$name;
                
            }
        }
        if ($request->hasFile('pan_upload')) {
            
            if ($request->file('pan_upload')->isValid()) {  
                    $validated = $request->validate([
                        'pan_upload' => 'mimes:jpeg,png|max:1014',
                    ]);
                     $validated['name']=$request->email;
                    $extension = $request->pan_upload->extension();
                    /*/////////////////////////*/
                    $destinationPath = '/lco/'.$validated['name']; 
                            
                    if(!Storage::exists($destinationPath)) {
                        Storage::makeDirectory($destinationPath, 0775, true); //creates directory
                    } 
                /*/////////////////////////*/
                    $name="pan_upload.".$extension;
                    $request->pan_upload->storeAs($destinationPath, $name);

                $request->pan_upload=$destinationPath.$name;
                
            }
        }
        if ($request->hasFile('gstin_upload')) {
            
            if ($request->file('gstin_upload')->isValid()) {  
                    $validated = $request->validate([
                        'gstin_upload' => 'mimes:jpeg,png|max:1014',
                    ]);
                     $validated['name']=$request->email;
                    $extension = $request->gstin_upload->extension();
                    /*/////////////////////////*/
                    $destinationPath = '/lco/'.$validated['name']; 
                            
                    if(!Storage::exists($destinationPath)) {
                        Storage::makeDirectory($destinationPath, 0775, true); //creates directory
                    } 
                /*/////////////////////////*/
                    $name="gstin_upload.".$extension;
                    $request->gstin_upload->storeAs($destinationPath, $name);

                $request->gstin_upload=$destinationPath.$name;
                
            }
        }
        /*///////////////////////////////*/
        $lcoData->update($request->all());
         echo"<pre>",print_r($request->all()),"<pre>";echo "<hr>";
        die($request->all());
        //return redirect()->route('lco.profile')->with('success',$lcoData->name.' LCO updated successfully');
    }
    public function storeFile($value)
    {
        # code...
    }
    public function storeImageOrignal (Request $request) {

        if ($request->hasFile('image')) {
            //  Let's do everything here
            if ($request->file('image')->isValid()) {
                //
                $validated = $request->validate([
                    'name' => 'string|max:40',
                    'image' => 'mimes:jpeg,png|max:1014',
                ]);
                $extension = $request->image->extension();
                $request->image->storeAs('/public', $validated['name'].".".$extension);
                $url = Storage::url($validated['name'].".".$extension);
                $file = File::create([
                   'name' => $validated['name'],
                    'url' => $url,
                ]);
                Session::flash('success', "Success!");
                return \Redirect::back();
            }
        }
        abort(500, 'Could not upload image :(');
    }
    
    public function destroy($id)
    {
        //
    }
    public function logout(Request $request)
    {

        Auth::guard('lco')->logout();

        $request->session()->flush();

        $request->session()->regenerate();

        return redirect()->guest(route( 'lco.login' ));
    }
}
