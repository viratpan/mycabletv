<?php

namespace App\Http\Controllers;

use App\Categorie;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Storage;

class CategorieController extends Controller
{
    function __construct()
    {
        $this->middleware('auth:admin');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($id=null)
    {
        $stateData =Categorie::find($id);
        $data = Categorie::all();
        return view('categorie.index', compact('data','stateData'));
    }

    

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request,$id=null)
    {
        $request->validate([
            'name' => ['required', 'string','unique:states'],          
        ]);
        Categorie::create($request->all());
        //die($password);
        return redirect()->route('admin.categorie')->with('success','Categorie created successfully.');
    }


   
    public function update(Request $request, $id)
    {
        $lcoData = Categorie::find($id);
        $lcoData->name = request('name');       
        $lcoData->save();
        $request->validate([
            'name' => ['required', 'string'],
           
         ]);
        $lcoData->update($request->all());
       
        return redirect()->route('admin.categorie')
                        ->with('success',$lcoData->name.'  updated successfully');
    }

    
    public function destroy($id)
    {
         Categorie::find($id)->delete();   
        //$Lco = Lco::withTrashed()->get();
           
        return redirect()->route('admin.categorie')
                        ->with('success','Categorie deleted successfully');
    }
}
