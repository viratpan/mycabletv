<?php

namespace App\Http\Controllers;

use App\State;
use App\City;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Storage;

class CityController extends Controller
{
    function __construct()
    {
        $this->middleware('auth:admin');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($id=null)
    {
        $stateData =City::find($id);
        //$data = City::all();
        $data=DB::table('cities')
                ->join('states','states.id','=','cities.states_id')
                ->select('cities.*','states.name as states_name')
                ->get();
        $states = State::all();
       
        return view('city.index', compact('data','stateData','states'));
    }

    

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request,$id=null)
    {
        $request->validate([
            'name' => ['required', 'string','unique:states'],
            'states_id' => ['required', ],          
        ]);
        City::create($request->all());
        //die($password);
        return redirect()->route('admin.city')->with('success','City created successfully.');
    }


   
    public function update(Request $request, $id)
    {
        $lcoData = City::find($id);
        $lcoData->name = request('name');       
        $lcoData->save();
        $request->validate([
            'name' => ['required', 'string'],
            'states_id' => ['required', ], 
         ]);
        $lcoData->update($request->all());
       
        return redirect()->route('admin.city')
                        ->with('success',$lcoData->name.'  updated successfully');
    }

    
    public function destroy($id)
    {
         City::find($id)->delete();   
        //$Lco = Lco::withTrashed()->get();
           
        return redirect()->route('admin.city')
                        ->with('success','City deleted successfully');
    }

    public function getCityList(Request $request)
    {
        $cities = DB::table("cities")
                    ->where("state_id",$request->state_id)
                    ->lists("name","id");
        return response()->json($cities);
    }
}
