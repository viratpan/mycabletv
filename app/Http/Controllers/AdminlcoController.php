<?php

namespace App\Http\Controllers;
use App\Lco;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Storage;

class AdminlcoController extends Controller
{
    public function __construct(Request $request)
    {
        $this->middleware('auth:admin');
        //die($request->all());
       /* $inputs=$request->all();
        foreach ($inputs as $key => $value) {
            echo "'$key', ";
        }
        die('run');*/
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $lcoData = Lco::all();
        return view('lco.index', compact('lcoData'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('lco.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {   
        //echo"<pre>",print_r($request->all()),"<pre>";echo "<hr>";
        //echo"<pre>",print_r($request->allFiles()),"<pre>";echo "<hr>";
        //die();
        $request->validate([
            'name' => ['required', 'string'],
            'email' => ['required', 'string', 'email', 'max:255', 'unique:lcos'],
            'mobile' => 'required',
        ]);
        $password=Str::random(10);
        $request->request->add(['password' =>  Hash::make( $password )]);

        $email=$request->email;

        if ($request->hasFile('registration_upload')) {
            
            if ($request->file('registration_upload')->isValid()) {  
                    $validated = $request->validate([
                        'registration_upload' => 'mimes:jpeg,png|max:1014',
                    ]);
                    
                    $extension = $request->registration_upload->extension();
                    /*/////////////////////////*/
                    $destinationPath = '/lco/'.$email; 
                            
                    if(!Storage::exists($destinationPath)) {
                        Storage::makeDirectory($destinationPath, 0775, true); //creates directory
                    } 
                /*/////////////////////////*/
                    $name="registration_upload.".$extension;
                    $request->registration_upload->storeAs($destinationPath, $name);

                $registration_upload=$destinationPath.'/'.$name;
                unset($request['registration_upload']);
                $request->request->add(['registration_upload' =>  $registration_upload]);
            }
        }
        if ($request->hasFile('postal_upload')) {
            
            if ($request->file('postal_upload')->isValid()) {  
                    $validated = $request->validate([
                        'postal_upload' => 'mimes:jpeg,png|max:1014',
                    ]);
                     
                    $extension = $request->postal_upload->extension();
                    /*/////////////////////////*/
                     $destinationPath = '/lco/'.$email; 
                            
                    if(!Storage::exists($destinationPath)) {
                        Storage::makeDirectory($destinationPath, 0775, true); //creates directory
                    } 
                /*/////////////////////////*/
                    $name="postal_upload.".$extension;
                    $request->postal_upload->storeAs($destinationPath, $name);

                $postal_upload=$destinationPath.'/'.$name;
                unset($request['postal_upload']);
                $request->request->add(['postal_upload' =>  $postal_upload]);
                
            }
        }
        if ($request->hasFile('pan_upload')) {
            
            if ($request->file('pan_upload')->isValid()) {  
                    $validated = $request->validate([
                        'pan_upload' => 'mimes:jpeg,png|max:1014',
                    ]);
                   
                    $extension = $request->pan_upload->extension();
                    /*/////////////////////////*/
                     $destinationPath = '/lco/'.$email;
                            
                    if(!Storage::exists($destinationPath)) {
                        Storage::makeDirectory($destinationPath, 0775, true); //creates directory
                    } 
                /*/////////////////////////*/
                    $name="pan_upload.".$extension;
                    $request->pan_upload->storeAs($destinationPath, $name);

                $pan_upload=$destinationPath.'/'.$name;
                unset($request['pan_upload']);
                $request->request->add(['pan_upload' =>  $pan_upload]);
                
            }
        }
        if ($request->hasFile('gstin_upload')) {
            
            if ($request->file('gstin_upload')->isValid()) {  
                    $validated = $request->validate([
                        'gstin_upload' => 'mimes:jpeg,png|max:1014',
                    ]);
                   
                    $extension = $request->gstin_upload->extension();
                    /*/////////////////////////*/
                     $destinationPath = '/lco/'.$email;
                            
                    if(!Storage::exists($destinationPath)) {
                        Storage::makeDirectory($destinationPath, 0775, true); //creates directory
                    } 
                /*/////////////////////////*/
                    $name=date('mdYHis') . uniqid() ."gstin_upload.".$extension;
                    //$request->gstin_upload->storeAs($destinationPath, $name);
                    $request->gstin_upload->storeAs('/public', $name.".".$extension);
                    echo $url = Storage::url($name.".".$extension);
                //echo $gstin_upload=$destinationPath.'/'.$name;
                $request->request->remove('gstin_upload');
               
               // echo $request->request->add(['gstin_upload' =>  $gstin_upload]);
                
            }
        }

        //Lco::create($request->all());
        echo"<pre>",print_r($request->all()),"<pre>";echo "<hr>";
        //return redirect()->route('admin.lco')->with('success','Lco created successfully.');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $lcoData = Lco::find($id);
        return view('lco.show',compact('lcoData'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
         $lcoData = Lco::find($id);
        return view('lco.update',compact('lcoData','id'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $lcoData = Lco::find($id);
        $lcoData->name = request('name');       
        $lcoData->save();
        $request->validate([
            'name' => ['required', 'string'],
            'email' => ['required', 'string', 'email', 'max:255'],
            'mobile' => 'required',
         ]);
        $lcoData->update($request->all());
        //dd(DB::getQueryLog());
        return redirect()->route('admin.lco.show',$id)
                        ->with('success',$lcoData->name.' LCO updated successfully');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
         Lco::find($id)->delete();   
        //$Lco = Lco::withTrashed()->get();
           
        return redirect()->route('admin.lco')
                        ->with('success','Lco deleted successfully');
    }
     public function storeImage (Request $request) {

        if ($request->hasFile('image')) {
            //  Let's do everything here
            if ($request->file('image')->isValid()) {
                //
                $validated = $request->validate([
                    'name' => 'string|max:40',
                    'image' => 'mimes:jpeg,png|max:1014',
                ]);
                $extension = $request->image->extension();
                $request->image->storeAs('/public', $validated['name'].".".$extension);
                $url = Storage::url($validated['name'].".".$extension);
                $file = File::create([
                   'name' => $validated['name'],
                    'url' => $url,
                ]);
                Session::flash('success', "Success!");
                return \Redirect::back();
            }
        }
        abort(500, 'Could not upload image :(');
    }
     public function storeImageOrignal (Request $request) {

        if ($request->hasFile('image')) {
            //  Let's do everything here
            if ($request->file('image')->isValid()) {
                //
                $validated = $request->validate([
                    'name' => 'string|max:40',
                    'image' => 'mimes:jpeg,png|max:1014',
                ]);
                $extension = $request->image->extension();
                $request->image->storeAs('/public', $validated['name'].".".$extension);
                $url = Storage::url($validated['name'].".".$extension);
                $file = File::create([
                   'name' => $validated['name'],
                    'url' => $url,
                ]);
                Session::flash('success', "Success!");
                return \Redirect::back();
            }
        }
        abort(500, 'Could not upload image :(');
    }
    public function upload()
    {
        $path = request()->image->store(config('wink.storage_path'), [
            'disk' => config('wink.storage_disk'),
            'visibility' => 'public',
        ]
        );

        return response()->json([
            'url' => Storage::disk(config('wink.storage_disk'))->url($path),
        ]);
    }

}
