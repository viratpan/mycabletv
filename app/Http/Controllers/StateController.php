<?php

namespace App\Http\Controllers;

use App\State;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Storage;

class StateController extends Controller
{
    function __construct()
    {
        $this->middleware('auth:admin');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($id=null)
    {
        $stateData =State::find($id);
        $data = State::all();
        return view('state.index', compact('data','stateData'));
    }

    

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request,$id=null)
    {
        $request->validate([
            'name' => ['required', 'string','unique:states'],          
        ]);
        State::create($request->all());
        //die($password);
        return redirect()->route('admin.state')->with('success','State created successfully.');
    }


   
    public function update(Request $request, $id)
    {
        $lcoData = State::find($id);
        $lcoData->name = request('name');       
        $lcoData->save();
        $request->validate([
            'name' => ['required', 'string'],
           
         ]);
        $lcoData->update($request->all());
       
        return redirect()->route('admin.state')
                        ->with('success',$lcoData->name.'  updated successfully');
    }

    
    public function destroy($id)
    {
         State::find($id)->delete();   
        //$Lco = Lco::withTrashed()->get();
           
        return redirect()->route('admin.state')
                        ->with('success','State deleted successfully');
    }
}
