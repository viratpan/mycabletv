<?php

namespace App\Http\Controllers;

use App\Categorie;
use App\Channel;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Storage;

class ChannelController extends Controller
{
    function __construct()
    {
        $this->middleware('auth:admin');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($id=null)
    {
        $tData =Channel::find($id);
        //$data = Channel::all();
        $data=DB::table('channels')
                ->join('categories','categories.id','=','channels.categories_id')
                ->select('channels.*','categories.name as categories_name')
                ->get();
        //die($data);
        $categorie = Categorie::all();
       
       return view('channel.index', compact('data','tData','categorie'));
    }

    

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request,$id=null)
    {
        $request->validate([
            'name' => ['required', 'string','unique:channels'],
            'channel_no' => ['required', 'string','unique:channels'],
            'price' => ['required', 'string',],
            'categories_id' => ['required', ],          
        ]);
        Channel::create($request->all());
        //die($password);
        return redirect()->route('admin.channel')->with('success','Channel created successfully.');
    }


   
    public function update(Request $request, $id)
    {
        $lcoData = Channel::find($id);
        $lcoData->name = request('name');       
        $lcoData->save();
        $request->validate([
            'name' => ['required', 'string',],
            'channel_no' => ['required', 'string',],
            'price' => ['required', 'string',],
            'categories_id' => ['required', ],
         ]);
        $lcoData->update($request->all());
       
        return redirect()->route('admin.channel')
                        ->with('success',$lcoData->name.'  updated successfully');
    }

    
    public function destroy($id)
    {
         Channel::find($id)->delete();   
        //$Lco = Lco::withTrashed()->get();
           
        return redirect()->route('admin.channel')
                        ->with('success','Channel deleted successfully');
    }

    public function getCityList(Request $request)
    {
        $cities = DB::table("cities")
                    ->where("state_id",$request->state_id)
                    ->lists("name","id");
        return response()->json($cities);
    }
}
