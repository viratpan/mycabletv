<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Providers\RouteServiceProvider;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Http\Request;
use Auth;
use Illuminate\Auth\SessionGuard;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = RouteServiceProvider::HOME;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        /*$this->middleware('guest')->except('logout');
        $this->middleware('guest:admin')->except('logout');
        $this->middleware('guest:lco')->except('logout');*/
    }
 /*
    admin 
 */   
    public function showAdminLoginForm()
    {
        return view('auth.login', ['url' => 'admin']);
    }

    public function adminLogin(Request $request)
    {
        $this->validate($request, [
            'email'   => 'required|email',
            'password' => 'required|min:6'
        ]);

        if (Auth::guard('admin')->attempt(['email' => $request->email, 'password' => $request->password], $request->get('remember'))) {

            return redirect()->intended('/admin');
        }
        return back()->withInput($request->only('email', 'remember'));
    }
/*
    LCO
*/
    public function showLcoLoginForm()
    {
        return view('auth.login', ['url' => 'lco']);
    }

    public function lcoLogin(Request $request)
    {
        $this->validate($request, [
            'email'   => 'required|email',
            'password' => 'required|min:6'
        ]);

        if (Auth::guard('lco')->attempt(['email' => $request->email, 'password' => $request->password], $request->get('remember'))) {

            return redirect()->intended('/lco');
        }
        return back()->withInput($request->only('email', 'remember'));
    }
    /*logout functions*/
    public function Adminlogout(Request $request)
    {
        Auth::guard('admin')->logout();

        $request->session()->invalidate();

        return redirect()->route('admin.dashboard')->with('loggedOut', true);
    }
    public function Lcologout(Request $request)
    {
        Auth::guard('lco')->logout();

        $request->session()->invalidate();

        return redirect()->route('lco')->with('loggedOut', true);
    }

    /*password reset*/
    public function showAdminResetRequestForm()
    {
        return view('auth.password.email', ['url' => 'admin']);
    }
    public function showLcoResetRequestForm()
    {
        return view('auth.password.email', ['url' => 'lco']);
    }
}
