<?php

namespace App\Http\Controllers;
use App\Mso;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class MsoController extends Controller
{
    public function __construct(Request $request)
    {
        $this->middleware('auth:admin');
        //die($request->all());
        
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //$msoData = Mso::latest()->paginate(10);
        $msoData = Mso::all();
        return view('mso.index', compact('msoData'));
      
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('mso.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {  // die($request);
        $request->validate([
            'name' => 'required',
            //'email' => 'required',
        ]);
  
        Mso::create($request->all());
        
        return redirect()->route('admin.mso')->with('success','MSO created successfully.');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $msoData = Mso::find($id);
        return view('mso.show',compact('msoData'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $msoData = Mso::find($id);
        return view('mso.update',compact('msoData','id'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $msoData = Mso::find($id);
        $msoData->name = request('name');       
        $msoData->save();
                $request->validate([
                'name' => 'required',
                //'email' => 'required',
         ]);
        $msoData->update($request->all());
  
        return redirect()->route('admin.mso')
                        ->with('success',$msoData->name.' MSO updated successfully');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {//die($id);
        Mso::find($id)->delete();   
        //$mso = Mso::withTrashed()->get();
           
        return redirect()->route('admin.mso')
                        ->with('success','MSO deleted successfully');
    }
    public function restore($id)
    {

        $mso = Mso::onlyTrashed()->find($id);

        if (!is_null($mso)) {

            $mso->restore();
            $response = $this->successfulMessage(200, 'Successfully restored', true, $note->count(), $note);
        } else {

            return response($response);
        }
        return response($response);
    }
    public function permanentDeleteSoftDeleted($id)
    {
        $note = Note::onlyTrashed()->find($id);

        if (!is_null($note)) {

            $note->forceDelete();
            $response = $this->successfulMessage(200, 'Successfully deleted', true, 0, $note);
        } else {

            return response($response);
        }
        return response($response);
    }
}
