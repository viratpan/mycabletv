<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Database\Eloquent\SoftDeletes; //add this for SoftDeletes

class Lco extends Authenticatable
{
    use Notifiable;
    use SoftDeletes; //add this for SoftDeletes


    protected $guard = 'lco';

    protected $dates = ['deleted_at'];

    protected $fillable = [       
            'name', 'email', 'email2', 'password', 'registration_no', 'authorized', 'designation', 'registration_from', 'mobile', 'mobile2', 'landline', 'registred_address', 'operational_address', 'postal_no', 'pan_no', 'gstin_no', 'registration_upload', 'postal_upload', 'pan_upload', 'gstin_upload', 'account_name', 'account_no', 'bank_name', 'bank_branch', 'bank_ifsc', 'remark',
    ];

    protected $hidden = [
        'password', 'remember_token',
    ];
}