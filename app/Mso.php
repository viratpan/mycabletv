<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;
use Illuminate\Database\Eloquent\SoftDeletes; //add this for SoftDeletes

class Mso extends Model
{
    use Notifiable;
    use SoftDeletes; //add this for SoftDeletes
    //protected $guard = 'lco';
    protected $dates = ['deleted_at'];
    protected $fillable = [
        'name', 'email', 'email2', 'registration_no', 'authorized', 'designation', 'issue_date', 'mobile', 'mobile2', 'landline', 'address', 'permitted_cities', 'account_name', 'account_no', 'bank_name', 'bank_branch', 'bank_ifsc', 'remark',
    ];

    
}
