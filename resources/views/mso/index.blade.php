@extends('layouts.app2')



@section('content')
<div class="card text-center">
  <div class="card-header">
    <h5 align="left">MSO
        <a class="btn btn-sm btn-info float-right" style="margin-right: 4rem;" href="{{ route('admin.mso.create') }}"><b>+</b></a>
    </h5>

  </div>
  <div class="card-body">
    <div class="table-responsive">
        <table id="data-table-basic" class="table">
            <thead>
                <tr>
                    <th>Registration No</th>
                    <th>Name</th>
                    <th>Email</th>
                    <th>Mobile</th>
                    <th>Permitted City/Town/Area Operation</th>
                    
                    <th>Action</th>
                </tr>
            </thead>
            <tbody>
            @foreach ($msoData as $uData)
                <tr>
                    <td>{{ $uData->registration_no }}</td>
                    <td>{{ $uData->name }}</td>
                    <td>{{ $uData->email }}</td>
                    <td>{{ $uData->mobile }}</td>
                    <td>{{ $uData->permitted_cities }}</td>
                    
                    <td>
                        <form action="{{ route('admin.mso.destroy',$uData->id) }}" method="POST">

                            <a class="btn btn-info" href="{{ route('admin.mso.show',$uData->id) }}">Show</a>

         
            
                            <a class="btn btn-primary" href="{{ route('admin.mso.edit',$uData->id) }}">Edit</a>
                            <!-- SUPPORT ABOVE VERSION 5.5 -->
                            {{-- @csrf
                            @method('DELETE') --}} 
                            
                            {{ csrf_field() }}
                            {{ method_field('DELETE') }}
                          
              
                            <button type="submit" class="btn btn-danger">Delete</button>
                        </form>
                      
                    </td>
                </tr>
            @endforeach    
                
            </tbody>
            <tfoot>
                <tr>
                   <th>Registration No</th>
                    <th>Name</th>
                    <th>Email</th>
                    <th>Mobile</th>
                    <th>Permitted City/Town/Area Operation</th>
                    
                    <th>Action</th>
                </tr>
            </tfoot>
        </table>

        
    </div>
  </div>
  <div class="card-footer text-muted">
{{--     {!! $uData->links() !!} --}}
{{--  {{ $uData->links() }} --}}
  </div>
</div>
	
   @endsection
