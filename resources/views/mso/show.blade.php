@extends('layouts.app2')



@section('content')
<style type="text/css">
	label {
    font-weight: bold;
}
</style>
<div class="card text-center">
  <div class="card-header">
    <h5 align="left">
    	<strong style="">{{ ucfirst($msoData->name) }}</strong > -MSO Details
        <a class="btn btn-sm btn-info float-right" style="margin-right: 4rem;" href="{{ route('admin.mso.edit',$msoData->id) }}"><b>Edit</b></a>
    </h5>

  </div>
  <div class="card-body">
  	<div class="form-row">
        <div class="form-group col-md-3">
          <label for="inputEmail4">Name</label>
          <br>{{ $msoData->name }}
        </div>
        <div class="form-group col-md-3">
          <label for="inputPassword4">Email</label>
         <br>{{ $msoData->email }}
        </div>
        <div class="form-group col-md-3">
          <label for="inputPassword4">Email 2</label>
        <br>{{ $msoData->email2 }}
        </div>
        <div class="form-group col-md-3">
          <label for="inputPassword4">Registration No</label>
          <br>{{ $msoData->registration_no }}
        </div>
{{-- Row 2 --}}
         <div class="form-group col-md-3">
          <label for="inputPassword4">Authorized Representative</label>
         <br>{{ $msoData->authorized }}
        </div>
        <div class="form-group col-md-3">
          <label for="inputPassword4">Designation </label>
        <br>{{ $msoData->designation }}
        </div>
         <div class="form-group col-md-3">
          <label for="inputPassword4">Issue Date</label>
         <br>{{ $msoData->issue_date }}
        </div>
        <div class="form-group col-md-3">
          <label for="inputPassword4">Phone </label>
           <br>{{ $msoData->mobile }}
        </div>
{{-- ror 3 --}}
         <div class="form-group col-md-3">
          <label for="inputPassword4">Phone 2</label>
          <br>{{ $msoData->mobile2 }}
        </div>
        <div class="form-group col-md-3">
          <label for="inputPassword4">Landline</label>
       <br>{{ $msoData->landline }}
        </div>
         <div class="form-group col-md-3">
          <label for="inputPassword4">Address</label>
         <br>{{ $msoData->address }}
        </div>
        <div class="form-group col-md-3">
          <label for="inputPassword4">Permitted City/Town/Area Operation </label>
        <br>{{ $msoData->permitted_cities }}
        </div>
<hr class="col-12">
    {{-- ror 4 --}}
         <div class="form-group col-md-3">
          <label for="inputPassword4">Account Holder Name</label>
          <br>{{ $msoData->account_name }}
        </div>
        <div class="form-group col-md-3">
          <label for="inputPassword4">Account NO</label>
        <br>{{ $msoData->account_no }}
        </div>
         <div class="form-group col-md-3">
          <label for="inputPassword4">Bank Name</label>
        <br>{{ $msoData->bank_name }}
        </div>
        <div class="form-group col-md-3">
          <label for="inputPassword4">Account Branch </label>
          <br>{{ $msoData->bank_branch }}
        </div>
        {{--  --}}
        <div class="form-group col-md-3">
          <label for="inputPassword4">IFSC Code </label>
           <br>{{ $msoData->bank_ifsc }}
        </div>
        <div class="form-group col-md-9">
          <label for="inputPassword4">Remark </label>
            <br>{{ $msoData->remark }}
        </div>

      </div>
      <div class="form-row float-right">
          <button type="submit" class="btn btn-primary ">Save</button>
      </div>
  </div>
  </div>
  <div class="card-footer text-muted">
    {{-- {!! $uData->links() !!} --}}

  </div>
</div>
	
   @endsection
