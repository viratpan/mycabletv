@extends('layouts.app2')



@section('content')
<div class="card text-center">
  <div class="card-header">
    <h5 align="left"><strong style="">{{ ucfirst($msoData->name) }}</strong > - MSO
        <a class="btn btn-sm btn-info float-right" style="margin-right: 4rem;" href="{{ route('admin.mso') }}"><b>Back</b></a>
    </h5>

  </div>
  <div class="card-body">
    {{--  --}}
    <form method="PUT" action="{{ route('admin.mso.update',$msoData->id) }}">
       @csrf
      
      <div class="form-row">
        <div class="form-group col-md-3">
          <label for="inputEmail4">Name</label>
          <input type="text" value="{{ $msoData->name }}" name="name" class="form-control" required placeholder="Name">
        </div>
        <div class="form-group col-md-3">
          <label for="inputPassword4">Email</label>
         <input type="email" value="{{ $msoData->email }}" name="email" class="form-control" required=""  placeholder="Email">
        </div>
        <div class="form-group col-md-3">
          <label for="inputPassword4">Email 2</label>
         <input type="email" value="{{ $msoData->email2 }}" name="email2" class="form-control"   placeholder="Email">
        </div>
        <div class="form-group col-md-3">
          <label for="inputPassword4">Registration No</label>
          <input type="text" value="{{ $msoData->registration_no }}" class="form-control" name="registration_no" placeholder="XXXXXX">
        </div>
{{-- Row 2 --}}
         <div class="form-group col-md-3">
          <label for="inputPassword4">Authorized Representative</label>
         <input type="text" value="{{ $msoData->authorized }}" name="authorized" class="form-control"  placeholder="Authorized Representative">
        </div>
        <div class="form-group col-md-3">
          <label for="inputPassword4">Designation </label>
         <input type="text" class="form-control" name="designation"  value="{{ $msoData->designation }}" placeholder="Designation ">
        </div>
         <div class="form-group col-md-3">
          <label for="inputPassword4">Issue Date</label>
         <input type="date" class="form-control" value="{{ $msoData->issue_date }}" name="issue_date" placeholder="XXXXXX">
        </div>
        <div class="form-group col-md-3">
          <label for="inputPassword4">Phone </label>
           <input type="text" name="mobile" value="{{ $msoData->mobile }}" class="form-control"  placeholder="Phone">
        </div>
{{-- ror 3 --}}
         <div class="form-group col-md-3">
          <label for="inputPassword4">Phone 2</label>
           <input type="text" name="mobile2" value="{{ $msoData->mobile2 }}" class="form-control"  placeholder="Phone">
        </div>
        <div class="form-group col-md-3">
          <label for="inputPassword4">Landline</label>
         <input type="text" name="landline" value="{{ $msoData->landline }}" class="form-control"  placeholder="landline">
        </div>
         <div class="form-group col-md-3">
          <label for="inputPassword4">Address</label>
         <input type="text" class="form-control" value="{{ $msoData->address }}" name="address" placeholder="Address">
        </div>
        <div class="form-group col-md-3">
          <label for="inputPassword4">Permitted City/Town/Area Operation </label>
          <input type="text" class="form-control" value="{{ $msoData->permitted_cities }}" name="permitted_cities" placeholder="Permitted Cities">
        </div>
<hr class="col-12">
    {{-- ror 4 --}}
         <div class="form-group col-md-3">
          <label for="inputPassword4">Account Holder Name</label>
          <input type="text" class="form-control" value="{{ $msoData->account_name }}" name="account_name" placeholder="Account Holder Name">
        </div>
        <div class="form-group col-md-3">
          <label for="inputPassword4">Account NO</label>
         <input type="text" class="form-control" value="{{ $msoData->account_no }}" name="account_no" placeholder="Account NO ">
        </div>
         <div class="form-group col-md-3">
          <label for="inputPassword4">Bank Name</label>
         <input type="text" class="form-control" value="{{ $msoData->bank_name }}" name="bank_name" placeholder="Bank Name">
        </div>
        <div class="form-group col-md-3">
          <label for="inputPassword4">Account Branch </label>
           <input type="text" class="form-control" name="bank_branch" placeholder="Account Branch " value="{{ $msoData->bank_branch }}">
        </div>
        {{--  --}}
        <div class="form-group col-md-3">
          <label for="inputPassword4">IFSC Code </label>
           <input type="text" class="form-control" name="bank_ifsc" placeholder="IFSC Code " value="{{ $msoData->bank_ifsc }}">
        </div>
        <div class="form-group col-md-9">
          <label for="inputPassword4">Remark </label>
            <input type="text" class="form-control" name="remark" placeholder="Remark  "  value="{{ $msoData->remark }}">
        </div>

      </div>
      <div class="form-row float-right">
          <button type="submit" class="btn btn-primary ">Save</button>
      </div>
    </form>
    {{--  --}}
  </div>
  <div class="card-footer text-muted">
    {{-- {!! $uData->links() !!} --}}
  </div>
</div>
    
@endsection
