@extends('layouts.app2')



@section('content')
<div class="card text-center">
  <div class="card-header">
    <h5 align="left">LCO
        <a class="btn btn-sm btn-info float-right" style="margin-right: 4rem;" href="{{ route('admin.lco') }}"><b>Back</b></a>
    </h5>

  </div>
  <div class="card-body">
    {{--  --}}
    <form method="POST" action="{{ route('admin.lco.store') }}" enctype="multipart/form-data">
        @csrf
      <div class="form-row">
        <div class="form-group col-md-3">
          <label for="inputEmail4">Name</label>
          <input type="text" value="{{ old('name') }}" name="name" class="form-control" required placeholder="Name">
        </div>
        <div class="form-group col-md-3">
          <label for="inputPassword4">Email</label>
         <input type="email" value="{{ old('email') }}" name="email" class="form-control" required=""  placeholder="Email">
        </div>
        <div class="form-group col-md-3">
          <label for="inputPassword4">Email 2</label>
         <input type="email" value="{{ old('email2') }}" name="email2" class="form-control"   placeholder="Email">
        </div>
        <div class="form-group col-md-3">
          <label for="inputPassword4">Organization Type </label>
          <input type="text" value="{{ old('organization_type') }}" class="form-control" name="registration_no" placeholder="XXXXXX">
        </div>
{{-- Row 2 --}}
         <div class="form-group col-md-3">
          <label for="inputPassword4">Authorized Representative</label>
         <input type="text" value="{{ old('authorized') }}" name="authorized" class="form-control"  placeholder="Authorized Representative">
        </div>
        <div class="form-group col-md-3">
          <label for="inputPassword4">Designation </label>
         <input type="text" class="form-control" name="designation"  value="{{ old('designation') }}" placeholder="Designation ">
        </div>
         <div class="form-group col-md-3">
          <label for="inputPassword4">Registration From</label>
         <input type="date" class="form-control" value="{{ old('registration_from') }}" name="registration_from" placeholder="XXXXXX">
        </div>
        <div class="form-group col-md-3">
          <label for="inputPassword4">Phone </label>
           <input type="text" required name="mobile" value="{{ old('mobile') }}" class="form-control"  placeholder="Phone">
        </div>
{{-- ror 3 --}}
         <div class="form-group col-md-3">
          <label for="inputPassword4">Phone 2</label>
           <input type="text" name="mobile2" value="{{ old('mobile2') }}" class="form-control"  placeholder="Phone 2">
        </div>
        <div class="form-group col-md-3">
          <label for="inputPassword4">Landline</label>
         <input type="text" name="landline" value="{{ old('landline') }}" class="form-control"  placeholder="landline">
        </div>
         <div class="form-group col-md-3">
          <label for="inputPassword4">Registred Address</label>
         <input type="text" class="form-control" value="{{ old('registred_address') }}" name="registred_address" placeholder="Registred Address">
        </div>
        <div class="form-group col-md-3">
          <label for="inputPassword4">Operational Address</label>
          <input type="text" class="form-control" value="{{ old('operational_address') }}" name="operational_address" placeholder="Operational Address">
        </div>
  {{-- ror 31 --}}
  <hr class="col-12">
         <div class="form-group col-md-3">
          <label for="inputPassword4">Registration No</label>
           <input type="text" name="registration_no" value="{{ old('registration_no') }}" class="form-control"  placeholder="Registration No">
        </div>
        <div class="form-group col-md-3">
          <label for="inputPassword4">Postal License No</label>
         <input type="text" name="postal_no" value="{{ old('postal_no') }}" class="form-control"  placeholder="Postal License No">
        </div>
         <div class="form-group col-md-3">
          <label for="inputPassword4">PAN License No </label>
         <input type="text" class="form-control" value="{{ old('pan_no') }}" name="pan_no" placeholder="PAN License No ">
        </div>
        <div class="form-group col-md-3">
          <label for="inputPassword4">GSTIN No </label>
          <input type="text" class="form-control" value="{{ old('gstin_no') }}" name="gstin_no" placeholder="GSTIN No ">
        </div>

        {{-- upload --}}
        <div class="form-group col-md-3">
          
           <input type="file" name="registration_upload" value="{{ old('registration_upload') }}" class="form-control"  placeholder="Registration No">
        </div>
        <div class="form-group col-md-3">
         
         <input type="file" name="postal_upload" value="{{ old('postal_upload') }}" class="form-control"  placeholder="Postal License No">
        </div>
         <div class="form-group col-md-3">
          
         <input type="file" class="form-control" value="{{ old('pan_upload') }}" name="pan_upload" placeholder="PAN License No ">
        </div>
        <div class="form-group col-md-3">
          
          <input type="file" class="form-control" value="{{ old('gstin_upload') }}" name="gstin_upload" placeholder="GSTIN No ">
        </div>
<hr class="col-12">
    {{-- ror 4 --}}
         <div class="form-group col-md-3">
          <label for="inputPassword4">Account Holder Name</label>
          <input type="text" class="form-control" value="{{ old('account_name') }}" name="account_name" placeholder="Account Holder Name">
        </div>
        <div class="form-group col-md-3">
          <label for="inputPassword4">Account NO</label>
         <input type="text" class="form-control" value="{{ old('account_no') }}" name="account_no" placeholder="Account NO ">
        </div>
         <div class="form-group col-md-3">
          <label for="inputPassword4">Bank Name</label>
         <input type="text" class="form-control" value="{{ old('bank_name') }}" name="bank_name" placeholder="Bank Name">
        </div>
        <div class="form-group col-md-3">
          <label for="inputPassword4">Account Branch </label>
           <input type="text" class="form-control" name="bank_branch" placeholder="Account Branch " value="{{ old('bank_branch') }}">
        </div>
        {{--  --}}
        <div class="form-group col-md-3">
          <label for="inputPassword4">IFSC Code </label>
           <input type="text" class="form-control" name="bank_ifsc" placeholder="IFSC Code " value="{{ old('bank_ifsc') }}">
        </div>
        <div class="form-group col-md-9">
          <label for="inputPassword4">Remark </label>
            <input type="text" class="form-control" name="remark" placeholder="Remark  "  value="{{ old('remark') }}">
        </div>

      </div>
      
      <div class="form-row float-right">
          <button type="submit" class="btn btn-primary ">Save</button>
      </div>
    </form>
    {{--  --}}
  </div>
  <div class="card-footer text-muted">
    {{-- {!! $uData->links() !!} --}}
  </div>
</div>
    
@endsection
