@extends('layouts.app2')



@section('content')
<div class="card text-center">
  <div class="card-header">
    <h5 align="left"><strong style="">{{ ucfirst($lcoData->name) }}</strong > - LCO
        <a class="btn btn-sm btn-info float-right" style="margin-right: 4rem;" href="{{ route('admin.lco.show',$lcoData->id) }}"><b>Back</b></a>
    </h5>

  </div>
  <div class="card-body">
    {{--  --}}
    <form method="PUT" action="{{ route('admin.lco.update',$lcoData->id) }}">
       @csrf
      
      <div class="form-row">
        <div class="form-group col-md-3">
          <label for="inputEmail4">Name</label>
          <input type="text" value="{{ $lcoData->name }}" name="name" class="form-control" required placeholder="Name">
        </div>
        <div class="form-group col-md-3">
          <label for="inputPassword4">Email</label>
         <input type="email" value="{{ $lcoData->email }}" name="email" class="form-control" required=""  placeholder="Email">
        </div>
        <div class="form-group col-md-3">
          <label for="inputPassword4">Email 2</label>
         <input type="email" value="{{ $lcoData->email2 }}" name="email2" class="form-control"   placeholder="Email">
        </div>
        <div class="form-group col-md-3">
          <label for="inputPassword4">Organization Type </label>
          <input type="text" value="{{ $lcoData->organization_type }}" class="form-control" name="registration_no" placeholder="XXXXXX">
        </div>
{{-- Row 2 --}}
         <div class="form-group col-md-3">
          <label for="inputPassword4">Authorized Representative</label>
         <input type="text" value="{{ $lcoData->authorized }}" name="authorized" class="form-control"  placeholder="Authorized Representative">
        </div>
        <div class="form-group col-md-3">
          <label for="inputPassword4">Designation </label>
         <input type="text" class="form-control" name="designation"  value="{{ $lcoData->designation }}" placeholder="Designation ">
        </div>
         <div class="form-group col-md-3">
          <label for="inputPassword4">Registration From</label>
         <input type="date" class="form-control" value="{{ $lcoData->registration_from }}" name="registration_from" placeholder="XXXXXX">
        </div>
        <div class="form-group col-md-3">
          <label for="inputPassword4">Phone </label>
           <input type="text" required name="mobile" value="{{ $lcoData->mobile }}" class="form-control"  placeholder="Phone">
        </div>
{{-- ror 3 --}}
         <div class="form-group col-md-3">
          <label for="inputPassword4">Phone 2</label>
           <input type="text" name="mobile2" value="{{ $lcoData->mobile2 }}" class="form-control"  placeholder="Phone 2">
        </div>
        <div class="form-group col-md-3">
          <label for="inputPassword4">Landline</label>
         <input type="text" name="landline" value="{{ $lcoData->landline }}" class="form-control"  placeholder="landline">
        </div>
         <div class="form-group col-md-3">
          <label for="inputPassword4">Registred Address</label>
         <input type="text" class="form-control" value="{{ $lcoData->registred_address }}" name="registred_address" placeholder="Registred Address">
        </div>
        <div class="form-group col-md-3">
          <label for="inputPassword4">Operational Address</label>
          <input type="text" class="form-control" value="{{ $lcoData->operational_address }}" name="operational_address" placeholder="Operational Address">
        </div>
  {{-- ror 31 --}}
  <hr class="col-12">
         <div class="form-group col-md-3">
          <label for="inputPassword4">Registration No</label>
           <input type="text" name="registration_no" value="{{ $lcoData->registration_no }}" class="form-control"  placeholder="Registration No">
        </div>
        <div class="form-group col-md-3">
          <label for="inputPassword4">Postal License No</label>
         <input type="text" name="postal_no" value="{{ $lcoData->postal_no }}" class="form-control"  placeholder="Postal License No">
        </div>
         <div class="form-group col-md-3">
          <label for="inputPassword4">PAN License No </label>
         <input type="text" class="form-control" value="{{ $lcoData->pan_no }}" name="pan_no" placeholder="PAN License No ">
        </div>
        <div class="form-group col-md-3">
          <label for="inputPassword4">GSTIN No </label>
          <input type="text" class="form-control" value="{{ $lcoData->gstin_no }}" name="gstin_no" placeholder="GSTIN No ">
        </div>

        {{-- upload --}}
        <div class="form-group col-md-3">
          
           <input type="file" name="registration_upload" value="{{ $lcoData->registration_upload }}" class="form-control"  placeholder="Registration No">
        </div>
        <div class="form-group col-md-3">
         
         <input type="file" name="postal_upload" value="{{ $lcoData->postal_upload }}" class="form-control"  placeholder="Postal License No">
        </div>
         <div class="form-group col-md-3">
          
         <input type="file" class="form-control" value="{{ $lcoData->pan_upload }}" name="pan_upload" placeholder="PAN License No ">
        </div>
        <div class="form-group col-md-3">
          
          <input type="file" class="form-control" value="{{ $lcoData->gstin_upload }}" name="gstin_upload" placeholder="GSTIN No ">
        </div>
<hr class="col-12">
    {{-- ror 4 --}}
         <div class="form-group col-md-3">
          <label for="inputPassword4">Account Holder Name</label>
          <input type="text" class="form-control" value="{{ $lcoData->account_name }}" name="account_name" placeholder="Account Holder Name">
        </div>
        <div class="form-group col-md-3">
          <label for="inputPassword4">Account NO</label>
         <input type="text" class="form-control" value="{{ $lcoData->account_no }}" name="account_no" placeholder="Account NO ">
        </div>
         <div class="form-group col-md-3">
          <label for="inputPassword4">Bank Name</label>
         <input type="text" class="form-control" value="{{ $lcoData->bank_name }}" name="bank_name" placeholder="Bank Name">
        </div>
        <div class="form-group col-md-3">
          <label for="inputPassword4">Account Branch </label>
           <input type="text" class="form-control" name="bank_branch" placeholder="Account Branch " value="{{ $lcoData->bank_branch }}">
        </div>
        {{--  --}}
        <div class="form-group col-md-3">
          <label for="inputPassword4">IFSC Code </label>
           <input type="text" class="form-control" name="bank_ifsc" placeholder="IFSC Code " value="{{ $lcoData->bank_ifsc }}">
        </div>
        <div class="form-group col-md-9">
          <label for="inputPassword4">Remark </label>
            <input type="text" class="form-control" name="remark" placeholder="Remark  "  value="{{ $lcoData->remark }}">
        </div>

      </div>


      <div class="form-row float-right">
          <button type="submit" class="btn btn-primary ">Save</button>
      </div>
    </form>
    {{--  --}}
  </div>
  <div class="card-footer text-muted">
    {{-- {!! $uData->links() !!} --}}
  </div>
</div>
    
@endsection
