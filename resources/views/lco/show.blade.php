@extends('layouts.app2')



@section('content')
<style type="text/css">
	label {
    font-weight: bold;
}
</style>
<div class="card text-center">
  <div class="card-header">
    <h5 align="left">
    	<strong style="">{{ ucfirst($lcoData->name) }}</strong > -LCO Details
        <a class="btn btn-sm btn-info float-right" style="margin-right: 4rem;" href="{{ route('admin.lco.edit',$lcoData->id) }}"><b>Edit</b></a>
    </h5>

  </div>
  <div class="card-body">
  	<div class="form-row">
        <div class="form-group col-md-3">
          <label for="inputEmail4">Name</label>
         <br>{{ $lcoData->name }}
        </div>
        <div class="form-group col-md-3">
          <label for="inputPassword4">Email</label>
         <br>{{ $lcoData->email }}
        </div>
        <div class="form-group col-md-3">
          <label for="inputPassword4">Email 2</label>
         <br>{{ $lcoData->email2 }}
        </div>
        <div class="form-group col-md-3">
          <label for="inputPassword4">Organization Type </label>
         <br>{{ $lcoData->organization_type }}
        </div>
{{-- Row 2 --}}
         <div class="form-group col-md-3">
          <label for="inputPassword4">Authorized Representative</label>
        <br>{{ $lcoData->authorized }}
        </div>
        <div class="form-group col-md-3">
          <label for="inputPassword4">Designation </label>
         <br>{{ $lcoData->designation }}
        </div>
         <div class="form-group col-md-3">
          <label for="inputPassword4">Registration From</label>
         <br>{{ $lcoData->registration_from }}
        </div>
        <div class="form-group col-md-3">
          <label for="inputPassword4">Phone </label>
          <br>{{ $lcoData->mobile }}
        </div>
{{-- ror 3 --}}
         <div class="form-group col-md-3">
          <label for="inputPassword4">Phone 2</label>
         <br>{{ $lcoData->mobile2 }}
     </div>
        <div class="form-group col-md-3">
          <label for="inputPassword4">Landline</label>
         <br>{{ $lcoData->landline }}
        </div>
         <div class="form-group col-md-3">
          <label for="inputPassword4">Registred Address</label>
         <br>{{ $lcoData->registred_address }}
        </div>
        <div class="form-group col-md-3">
          <label for="inputPassword4">Operational Address</label>
          <br>{{ $lcoData->operational_address }}
        </div>
  {{-- ror 31 --}}
  <hr class="col-12">
         <div class="form-group col-md-3">
          <label for="inputPassword4">Registration No</label>
          <br>{{ $lcoData->registration_no }}
        </div>
        <div class="form-group col-md-3">
          <label for="inputPassword4">Postal License No</label>
        <br>{{ $lcoData->postal_no }}
        </div>
         <div class="form-group col-md-3">
          <label for="inputPassword4">PAN License No </label>
         <br>{{ $lcoData->pan_no }}
        </div>
        <div class="form-group col-md-3">
          <label for="inputPassword4">GSTIN No </label>
          <br>{{ $lcoData->gstin_no }}
        </div>

        {{-- upload --}}
        <div class="form-group col-md-3">
          <label for="inputPassword4">Registration No</label>
           <br>{{ $lcoData->registration_upload }}
        </div>
        <div class="form-group col-md-3">
          <label for="inputPassword4">Postal License No</label>
         <br>{{ $lcoData->postal_upload }}
        </div>
         <div class="form-group col-md-3">
          <label for="inputPassword4">PAN License No </label>
        <br>{{ $lcoData->pan_upload }}
        </div>
        <div class="form-group col-md-3">
          <label for="inputPassword4">GSTIN No </label>
         <br>{{ $lcoData->gstin_upload }}
        </div>
<hr class="col-12">
    {{-- ror 4 --}}
         <div class="form-group col-md-3">
          <label for="inputPassword4">Account Holder Name</label>
          <br>{{ $lcoData->account_name }}
        </div>
        <div class="form-group col-md-3">
          <label for="inputPassword4">Account NO</label>
         <br>{{ $lcoData->account_no }}
        </div>
         <div class="form-group col-md-3">
          <label for="inputPassword4">Bank Name</label>
         <br>{{ $lcoData->bank_name }}
        </div>
        <div class="form-group col-md-3">
          <label for="inputPassword4">Account Branch </label>
           <br>{{ $lcoData->bank_branch }}
        </div>
        {{--  --}}
        <div class="form-group col-md-3">
          <label for="inputPassword4">IFSC Code </label>
           <br>{{ $lcoData->bank_ifsc }}
        </div>
        <div class="form-group col-md-9">
          <label for="inputPassword4">Remark </label>
           <br>{{ $lcoData->remark }}
        </div>

      </div>
      
  </div>
  </div>
  <div class="card-footer text-muted">
    {{-- {!! $uData->links() !!} --}}

  </div>
</div>
	
   @endsection
