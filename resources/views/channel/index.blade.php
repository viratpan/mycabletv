@extends('layouts.app2')



@section('content')


<div class="card text-center">
  <div class="card-header">
    <div>
        <form method="{{ isset($tData) ? 'PUT': "POST"}}" action="{{ route('admin.channel') }}{{ isset($tData) ? '/update' : "/store"}}{{ isset($tData) ? '/'.$tData->id : ""}}">
          @csrf
          <div class="form-row">
              <div class="form-group col-md-4">
                  <label for="inputEmail4">Name</label>
                  <input type="text" value="{{ old('name') }}{{ isset($tData) ? $tData->name : ""}}" name="name" class="form-control" required placeholder="Channel Name">
              </div>
              <div class="form-group col-md-2">
                  <label for="inputEmail4">Channel No</label>
                  <input type="text" value="{{ old('channel_no') }}{{ isset($tData) ? $tData->channel_no : ""}}" name="channel_no" class="form-control" required placeholder="Channel No">
              </div>
              <div class="form-group col-md-2">
                  <label for="inputEmail4">Price </label>
                  <input type="number" value="{{ old('price') }}{{ isset($tData) ? $tData->price : ""}}" name="price" class="form-control" required placeholder="Price">
              </div>
              <div class="form-group col-md-2">
                  <label for="inputEmail4">Category</label>                 
                  <select value="{{ old('categories_id') }}" name="categories_id" class="form-control" required >
                    <option value="">Select Category</option>
                    @foreach($categorie as $svalue)
                      <option value="{{ $svalue->id }}" {{ isset($tData) ? ( $tData->categories_id==$svalue->id ? "Selected":"" ) : ""}}>{{ $svalue->name }}</option>
                    @endforeach
                  </select>
              </div>
              <div class="form-group col-md-2">
                  <label for="inputEmail4">Image </label>
                  <input type="file" value="{{ old('image') }}{{ isset($tData) ? $tData->image : ""}}" name="image" class="form-control"  placeholder="Price">
              </div>
              <div class="form-group col-md-12">
                  <label for="inputEmail4">Description </label>
                  <textarea name="description" class="form-control"  placeholder="Description">{{ old('description') }}{{ isset($tData) ? $tData->description : ""}}</textarea> 
              </div> 
              <div class="form-group col-md-4  float-right" style="">
                <label for="inputEmail4"></label>
                  <button type="submit" class="btn btn-primary ">{{ isset($tData) ? 'Save' : "Add"}} Channel</button>
                  @if(isset($tData) )
                  <a href="{{ route('admin.channel') }}" class="btn btn-info ">Cancel</a>
                  @endif
              </div>
          </div>                 
          
      </form>
    </div>

  </div>
  <div class="card-body">
    <?php //echo"<pre>",print_r($data),"</pre>";?>
    <div class="table-responsive">
        <table id="data-table-basic" class="table">
            <thead>
                <tr>
                    <th>Channel No</th>
                    <th>Name</th> 
                    <th>Price</th> 
                    <th>Category</th>                    
                    <th>Action</th>
                </tr>
            </thead>
            <tbody>
            @foreach ($data as $uData)
                <tr>
                    <td>{{ $uData->channel_no }}</td>
                    <td>{{ $uData->name }}</td>
                     <td>{{ $uData->price }}</td>
                    <td>{{ $uData->categories_name }}</td>
                    
                    <td>
                        <form action="{{ route('admin.channel.destroy',$uData->id) }}" method="POST">

                            <a class="btn btn-primary" href="{{ route('admin.channel.edit',$uData->id) }}">Edit</a>
                            <!-- SUPPORT ABOVE VERSION 5.5 -->
                            {{-- @csrf
                            @method('DELETE') --}} 
                            
                            {{ csrf_field() }}
                            {{ method_field('DELETE') }}
                          
              
                            <button type="submit" class="btn btn-danger">Delete</button>
                        </form>
                      
                    </td>
                </tr>
            @endforeach    
                
            </tbody>
            <tfoot>
                <tr>
                   <th>Channel No</th>
                    <th>Name</th> 
                    <th>Price</th> 
                    <th>Category</th>                    
                    <th>Action</th>
                </tr>
            </tfoot>
        </table>

        
    </div>
  </div>
  <div class="card-footer text-muted">
{{--     {!! $uData->links() !!} --}}
{{--  {{ $uData->links() }} --}}
  </div>
</div>
	
   @endsection
