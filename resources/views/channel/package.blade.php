@extends('layouts.app2')

@section('content')


<div class="card text-center">
  <div class="card-header">
    <div>
        <form method="{{ isset($tData) ? 'PUT': 'POST' }}" action="{{ route('admin.package') }}{{ isset($tData) ? '/update' : '/store'}}{{ isset($tData) ? '/'.$tData->id : ''}}">
          @csrf
          <div class="form-row">
              <div class="form-group col-md-8">
                  <label for="inputEmail4">Name</label>
                  <input type="text" value="{{ old('name') }}{{ isset($tData) ? $tData->name : ""}}" name="name" class="form-control" required placeholder="Package Name">
              </div>
              
              <div class="form-group col-md-2">
                  <label for="inputEmail4">Price </label>
                  <input type="number" value="{{ old('price') }}{{ isset($tData) ? $tData->price : ""}}" name="price" class="form-control" required placeholder="Price">
              </div>
             
              <div class="form-group col-md-2">
                  <label for="inputEmail4">Image </label>
                  <input type="file" value="{{ old('image') }}{{ isset($tData) ? $tData->image : ""}}" name="image" class="form-control"  placeholder="Price">
              </div>
              <div class="form-group col-md-12">
                  <label for="inputEmail4">Description </label>
                  <textarea name="description" class="form-control"  placeholder="Description">{{ old('description') }}{{ isset($tData) ? $tData->description : ""}}</textarea> 
              </div> 
              <div class="form-group col-md-4 " style="">
                <label for="inputEmail4"></label>
                  <button type="submit" class="btn btn-primary ">{{ isset($tData) ? 'Save' : "Add"}} Package</button>
                  @if(isset($tData) )
                  <a href="{{ route('admin.package') }}" class="btn btn-info ">Cancel</a>
                  @endif
              </div>
          </div>                 
          
      </form>
    </div>

  </div>
  <div class="card-body">
    <?php //echo"<pre>",print_r($data),"</pre>";?>
    <div class="table-responsive">
        <table id="data-table-basic" class="table">
            <thead>
                <tr>
                    <th>Id</th>
                    <th>Name</th> 
                    <th>Price</th> 
                    <th>Description</th>                    
                    <th>Action</th>
                </tr>
            </thead>
            <tbody>
            @foreach ($data as $uData)
                <tr>
                    <td>{{ $uData->id }}</td>
                    <td>{{ $uData->name }}</td>
                     <td>{{ $uData->price }}</td>
                    <td>{{ $uData->description  }}</td>
                    
                    <td>
                        <form action="{{ route('admin.package.destroy',$uData->id) }}" method="POST">

                            <a class="btn btn-primary" href="{{ route('admin.package.edit',$uData->id) }}">Edit</a>
                            <!-- SUPPORT ABOVE VERSION 5.5 -->
                            {{-- @csrf
                            @method('DELETE') --}} 
                            
                            {{ csrf_field() }}
                            {{ method_field('DELETE') }}
                          
              
                            <button type="submit" class="btn btn-danger">Delete</button>
                        </form>
                      
                    </td>
                </tr>
            @endforeach    
                
            </tbody>
            <tfoot>
                <tr>
                    <th>Id</th>
                    <th>Name</th> 
                    <th>Price</th> 
                    <th>Description</th>                    
                    <th>Action</th>
                </tr>
            </tfoot>
        </table>

        
    </div>
  </div>
  <div class="card-footer text-muted">
{{--     {!! $uData->links() !!} --}}
{{--  {{ $uData->links() }} --}}
  </div>
</div>
	
@endsection
