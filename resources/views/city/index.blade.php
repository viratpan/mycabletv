@extends('layouts.app2')



@section('content')


<div class="card text-center">
  <div class="card-header">
    <div>
        <form method="{{ isset($stateData) ? 'PUT': "POST"}}" action="{{ route('admin.city') }}{{ isset($stateData) ? '/update' : "/store"}}{{ isset($stateData) ? '/'.$stateData->id : ""}}">
          @csrf
          <div class="form-row">
              <div class="form-group col-md-5">
                  <label for="inputEmail4">Name</label>
                  <input type="text" value="{{ old('name') }}{{ isset($stateData) ? $stateData->name : ""}}" name="name" class="form-control" required placeholder="City Name">
              </div>
               <div class="form-group col-md-5">
                  <label for="inputEmail4">State</label>                 
                  <select value="{{ old('states_id') }}" name="states_id" class="form-control" required >
                    <option value="">Select State</option>
                    @foreach($states as $svalue)
                      <option value="{{ $svalue->id }}" {{ isset($stateData) ? ( $stateData->states_id==$svalue->id ? "Selected":"" ) : ""}}>{{ $svalue->name }}</option>
                    @endforeach
                  </select>
              </div>
              <div class="form-group col-md-2 float-right" style="padding-top: 2rem;">
                <label for="inputEmail4"></label>
                  <button type="submit" class="btn btn-primary ">Add City</button>
                  <a href="{{ route('admin.city') }}" class="btn btn-info ">Cancel</a>
              </div>
          </div>                 
          
      </form>
    </div>

  </div>
  <div class="card-body">
    <?php //echo"<pre>",print_r($data),"</pre>";?>
    <div class="table-responsive">
        <table id="data-table-basic" class="table">
            <thead>
                <tr>
                    <th>Id</th>
                    <th>Name</th> 
                    <th>State</th>                    
                    <th>Action</th>
                </tr>
            </thead>
            <tbody>
            @foreach ($data as $uData)
                <tr>
                    <td>{{ $uData->id }}</td>
                    <td>{{ $uData->name }}</td>
                    <td>{{ $uData->states_name  }}</td>
                    
                    <td>
                        <form action="{{ route('admin.city.destroy',$uData->id) }}" method="POST">

                            <a class="btn btn-primary" href="{{ route('admin.city.edit',$uData->id) }}">Edit</a>
                            <!-- SUPPORT ABOVE VERSION 5.5 -->
                            {{-- @csrf
                            @method('DELETE') --}} 
                            
                            {{ csrf_field() }}
                            {{ method_field('DELETE') }}
                          
              
                            <button type="submit" class="btn btn-danger">Delete</button>
                        </form>
                      
                    </td>
                </tr>
            @endforeach    
                
            </tbody>
            <tfoot>
                <tr>
                   <th>Id</th>
                    <th>Name</th>
                    <th>State</th>
                    <th>Action</th>
                </tr>
            </tfoot>
        </table>

        
    </div>
  </div>
  <div class="card-footer text-muted">
{{--     {!! $uData->links() !!} --}}
{{--  {{ $uData->links() }} --}}
  </div>
</div>
	
   @endsection
