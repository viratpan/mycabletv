<div class="sidebar-sticky">
            <ul class="nav flex-column">
              <li class="nav-item {!! (Route::is('admin.dashboard') ? 'active' : '') !!}">
                <a class="nav-link " href="{{ route('admin.dashboard')}}">
                  <span data-feather="home"></span>
                  Dashboard <span class="sr-only">(current)</span>
                </a>
              </li> 
              <li class="nav-item {!! (Route::is('admin.mso') ? 'active' : '') !!}">
                <a class="nav-link" href="{{ route('admin.mso')}}">
                  <span data-feather="file"></span>
                  MSO
                </a>
              </li>
              <li class="nav-item {!! (Route::is('admin.lco') ? 'active' : '') !!}">
                <a class="nav-link" href="{{ route('admin.lco')}}">
                  <span data-feather="file"></span>
                  LCO
                </a>
              </li>
              {{-- <li class="nav-item {!! (Route::is('admin.mso') ? 'active' : '') !!}">
                <a class="nav-link" href="{{ route('admin.mso')}}">
                  <span data-feather="file"></span>
                  MSO
                </a>
              </li> --}}
            </ul>
            <hr>
            <h6 class="sidebar-heading d-flex justify-content-between align-items-center px-3 mt-4 mb-1 text-muted">
              <span>Setting</span>
              <a class="d-flex align-items-center text-muted" href="#">
                <span data-feather="plus-circle"></span>
              </a>
            </h6>
            <ul class="nav flex-column mb-2">
              <li class="nav-item">
                <a class="nav-link" href="#">
                  <span data-feather="file-text"></span>
                  Current month
                </a>
              </li>
              <li class="nav-item">
                <a class="nav-link" href="#">
                  <span data-feather="file-text"></span>
                  Last quarter
                </a>
              </li>
              <li class="nav-item">
                <a class="nav-link" href="#">
                  <span data-feather="file-text"></span>
                  Social engagement
                </a>
              </li>
              <li class="nav-item">
                <a class="nav-link" href="#">
                  <span data-feather="file-text"></span>
                  Year-end sale
                </a>
              </li>
            </ul>
          </div>