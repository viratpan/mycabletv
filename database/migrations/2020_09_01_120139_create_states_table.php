<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateStatesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('states', function (Blueprint $table) {            
            $table->increments('id');            
            $table->string('name')->unique();
            $table->softDeletes();  //add this for Soft delete
            $table->timestamps();
        });
        Schema::create('cities', function (Blueprint $table) {
            $table->increments('id');            
            $table->string('name')->unique();
            //$table->uuid('states_id');
            $table->unsignedInteger('states_id');
            $table->foreign('states_id')->references('id')->on('states')->onDelete('cascade');
            //$table->foreign('states_id')->references('id')->on('states');
            $table->softDeletes();  //add this for Soft delete
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('states');
        Schema::dropIfExists('cities');
    }
}
