<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateLcosTable extends Migration
{
    /**
     * Run the migrations. 
     *
     * @return void
     */
    public function up()
    {
        Schema::create('lcos', function (Blueprint $table) {
            $table->bigIncrements('id');

            $table->string('name')->comment('LCO Name');
            $table->string('email')->unique()->comment('Email for login LCO');
            //$table->string('password')->nullable($value = true);
            $table->boolean('active')->default(0);
            $table->string('password');
           
            
            $table->string('organization_type')->nullable($value = true)->comment('Organization No( Individual, Property, Partnership Firm,Private Limited, LLP, Limited )');

            $table->string('registred_address')->nullable($value = true)->comment('Registred Address');
            $table->string('operational_address')->nullable($value = true)->comment('operational Address');

            $table->string('authorized')->nullable($value = true)->comment('Authorized Representative');
            $table->string('designation')->nullable($value = true)->comment('Designation No');

            $table->string('mobile')->nullable($value = true)->comment('mobile No 1');
            $table->string('mobile2')->nullable($value = true)->comment('mobile No 1');
            $table->string('landline')->nullable($value = true)->comment('landline No');

            
            $table->string('email2')->nullable($value = true)->comment('Email 2');

            

            $table->string('registration_from')->nullable($value = true)->comment('Registration From date');
              $table->string('registration_no')->nullable($value = true)->comment('Registration No');
                $table->string('registration_upload')->nullable($value = true)->comment('Upload Registration Certificte ');

             $table->string('postal_no')->nullable($value = true)->comment('Postal License  No');
                $table->string('postal_upload')->nullable($value = true)->comment('Upload Postal License Certificte ');

            $table->string('pan_no')->nullable($value = true)->comment('PAN License  No');
                $table->string('pan_upload')->nullable($value = true)->comment('Upload PAN License Certificte ');

            $table->string('gstin_no')->nullable($value = true)->comment('GSTIN No');
                $table->string('gstin_upload')->nullable($value = true)->comment('Upload GSTIN Certificte ');
                
            $table->string('account_name')->nullable($value = true)->comment('Account Holder Name 2');
            $table->string('account_no')->nullable($value = true)->comment('Account No');

            $table->string('bank_name')->nullable($value = true)->comment('Bank Name');
            $table->string('bank_branch')->nullable($value = true)->comment('Bank Branch');
            $table->string('bank_ifsc')->nullable($value = true)->comment('IFSC Code');

            
            $table->longText('remark')->nullable($value = true)->comment('Remark');

            $table->rememberToken();
             $table->softDeletes();  //add this for Soft delete
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('lcos');
    }
}
