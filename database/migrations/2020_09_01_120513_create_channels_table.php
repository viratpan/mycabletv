<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateChannelsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('categories', function (Blueprint $table) {
            $table->increments('id');            
            $table->string('name')->unique();
            $table->softDeletes();  //add this for Soft delete
            $table->timestamps();
        });
            Artisan::call('db:seed', [
            '--class' => CategorieSeed::class
        ]);
        Schema::create('channels', function (Blueprint $table) {
            $table->increments('id');            
            $table->string('name')->unique();
            $table->string('channel_no')->unique();
            $table->string('price')->default(0);
            $table->longText('description')->nullable($value = true);
            $table->mediumText('image')->nullable($value = true);
             //$table->uuid('categories_id');
             //$table->foreign('categories_id')->references('id')->on('categories');
              $table->unsignedInteger('categories_id');
            $table->foreign('categories_id')->references('id')->on('categories')->onDelete('cascade');
            $table->softDeletes();  //add this for Soft delete
            $table->timestamps();
        });

        Schema::create('packages', function (Blueprint $table) {           
            $table->increments('id');            
            $table->string('name')->unique();
            $table->string('price')->nullable($value = true);
            $table->longText('description')->nullable($value = true);
            $table->mediumText('image')->nullable($value = true);
            $table->json('channels')->nullable($value = true);
            $table->softDeletes();  //add this for Soft delete
            $table->timestamps();
        });
        Schema::create('package_channels', function (Blueprint $table) {           
            $table->increments('id');            
             $table->unsignedInteger('package_id');
            $table->foreign('package_id')->references('id')->on('packages')->onDelete('cascade');
             $table->unsignedInteger('channels_id');
            $table->foreign('channels_id')->references('id')->on('channels')->onDelete('cascade');
            $table->softDeletes();  //add this for Soft delete
            $table->timestamps();
        });


        
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('categories');
        Schema::dropIfExists('channels');
        Schema::dropIfExists('package');
        Schema::dropIfExists('package_channels');
    }
}
