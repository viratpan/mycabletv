<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateMsosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('msos', function (Blueprint $table) {
            $table->increments('id')->index();
            $table->string('name')->unique()->comment('MSO Name');
            $table->string('registration_no')->nullable($value = true)->comment('Registration No');
            $table->string('authorized')->nullable($value = true)->comment('Authorized Representative');
            $table->string('designation')->nullable($value = true)->comment('Designation No');

            $table->string('mobile')->nullable($value = true)->comment('mobile No 1');
            $table->string('mobile2')->nullable($value = true)->comment('mobile No 1');
            $table->string('landline')->nullable($value = true)->comment('landline No');

            $table->string('email')->nullable($value = true)->comment('Email 1');
            $table->string('email2')->nullable($value = true)->comment('Email 2');

             $table->string('address')->nullable($value = true)->comment('Address of MSO');

            $table->string('permitted_cities')->nullable($value = true)->comment('Permitted City/Town/Area Operation ');
            $table->string('issue_date')->nullable($value = true)->comment('Permission issued on (Date)');

            $table->string('account_name')->nullable($value = true)->comment('Account Holder Name 2');
            $table->string('account_no')->nullable($value = true)->comment('Account No');

            $table->string('bank_name')->nullable($value = true)->comment('Bank Name');
            $table->string('bank_branch')->nullable($value = true)->comment('Bank Branch');
            $table->string('bank_ifsc')->nullable($value = true)->comment('IFSC Code');
            $table->boolean('active')->default(0);
            $table->longText('remark')->nullable($value = true)->comment('Remark');


            $table->softDeletes();  //add this for Soft delete
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
       // Schema::dropIfExists('msos');
        Schema::table('msos', function (Blueprint $table) {

            $table->dropSoftDeletes(); //add this line
        });
    }
}
