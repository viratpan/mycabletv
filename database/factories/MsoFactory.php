<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Mso;
use Faker\Generator as Faker;
use Illuminate\Support\Str;

$factory->define(Mso::class, function (Faker $faker) {
    return [
        'name' => $faker->unique()->name,
        'email' => $faker->unique()->safeEmail,
        'email2' => $faker->safeEmail,
        'mobile' => $faker->unique()->phoneNumber,
        'mobile2' => $faker->phoneNumber,
        'landline' => $faker->phoneNumber, // "03-7112 0455"
        'address' => $faker->address,       
        'registration_no' => Str::random(10) ,
        'authorized' => $faker->randomElement($array = array ('Govt','Delhi','Pvt  WWW','Global')),
         'designation' => $faker->randomElement($array = array ('Self','DEV','LTD  PVT','MTC')),
         'permitted_cities' => $faker->randomElement($array = array ('Noida','Delhi','All  India','Jaipur')),
         'issue_date' => $faker->date($format = 'Y-m-d', $max = 'now'),

         'account_name' => $faker->bankAccountNumber,  
         'account_no' => $faker->creditCardNumber,  
         'bank_name' => $faker->address,  
         'bank_branch' => null,  
         'bank_ifsc' => null,  

        'remark' => $faker->realText($maxNbChars = 200, $indexSize = 2),
    ];
});
