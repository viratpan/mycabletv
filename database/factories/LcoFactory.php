<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Lco;
use Faker\Generator as Faker;
use Illuminate\Support\Str;

$factory->define(Lco::class, function (Faker $faker) {
    return [
        'name' => $faker->unique()->name,
        'email' => $faker->unique()->safeEmail,
        'email2' => $faker->safeEmail,
        'mobile' => $faker->unique()->phoneNumber,
        'mobile2' => $faker->phoneNumber,
        'landline' => $faker->phoneNumber, // "03-7112 0455"
      
        'organization_type' =>$faker->randomElement($array = array ('Individual', 'Property', 'Partnership','Firm','PrivateLimited', 'LLP', 'Limited' )),
        'authorized' => $faker->randomElement($array = array ('Govt','Delhi','Pvt  WWW','Global')),
         'designation' => $faker->randomElement($array = array ('Self','DEV','LTD  PVT','MTC')),
         'registred_address' => $faker->address,       
         'operational_address' => $faker->randomElement($array = array ('Noida','Delhi','All  India','Jaipur')),
         'registration_from' => $faker->date($format = 'Y-m-d', $max = 'now'),

         'account_name' => $faker->bankAccountNumber,  
         'account_no' => $faker->creditCardNumber,  
         'bank_name' => $faker->address,  
         'bank_branch' => null,  
         'bank_ifsc' => null,  

        'remark' => $faker->realText($maxNbChars = 200, $indexSize = 2),
    ];
});
