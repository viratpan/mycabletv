<?php

use Illuminate\Database\Seeder;

class CategorieSeed extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
    	DB::table('states')->insert([
            ['name' => 'Assam'],
            ['name' => 'Bihar'],
            ['name' => 'Chhattisgarh'],           
            ['name' => 'Goa'],
            ['name' => 'Gujarat'],
            ['name' => 'Haryana'],
            ['name' => 'Himachal Pradesh'],
            ['name' => 'Jammu and Kashmir'],
            ['name' => 'Jharkhand'],
            ['name' => 'Karnataka'],
            ['name' => 'Kerala'],
            ['name' => 'Madhya Pradesh'],
            ['name' => 'Maharashtra'],
            ['name' => 'Manipur'],
            ['name' => 'Meghalaya'],
            ['name' => 'Mizoram'],
            ['name' => 'Nagaland'],
            ['name' => 'Odisha (former Orissa)'],
            ['name' => 'Punjab'],
            ['name' => 'Rajasthan'],
            ['name' => 'Uttarakhand'],
            ['name' => 'Delhi'],
            ['name' => 'UttarPardesh'],
        ]);

        DB::table('categories')->insert([
            ['name' => 'Entertainment'],
            ['name' => 'Shutdown Channels'],
            ['name' => 'Movies'],           
            ['name' => 'Music'],
            ['name' => 'Youth and Bollywood'],
            ['name' => 'Kids'],
            ['name' => 'Infotainment'],
            ['name' => 'lifestyle'],
            ['name' => 'News and business'],
            ['name' => 'Religious'],
            ['name' => 'Sports'],
        ]);
    }
}
