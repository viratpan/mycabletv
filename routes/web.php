<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('/install', function () {
   	/* php artisan migrate */
    \Artisan::call('route:list');
     dd("Done");
});

Auth::routes();
Route::get('adminpassword/reset', 'Auth\LoginController@showAdminResetRequestForm');
Route::get('lcopassword/reset', 'Auth\LoginController@showLcoResetRequestForm');


Route::get('/home', 'HomeController@index')->name('home');

	Route::get('/login/admin', 'Auth\LoginController@showAdminLoginForm')->name('admin.login');
    Route::get('/login/lco', 'Auth\LoginController@showLcoLoginForm')->name('lco.login');
    	Route::get('/register/lco', 'Auth\RegisterController@showLcoRegisterForm');
   /* Route::get('/register/writer', 'Auth\RegisterController@showWriterRegisterForm');*/

    Route::post('/login/admin', 'Auth\LoginController@adminLogin');
    Route::post('/login/lco', 'Auth\LoginController@lcoLogin');
    	Route::post('/register/lco', 'Auth\RegisterController@createLco');
    /*
    Route::post('/register/writer', 'Auth\RegisterController@createWriter');*/
/*Route::get('/admin', 'AdminController@index')->name('admin.dashboard');
Route::get('/lco', 'LcoController@index')->name('lco.dashboard');*/

Route::prefix('admin')->group(function () {
    Route::get('/', 'AdminController@index')->name('admin.dashboard');
    Route::post('/logout', 'Auth\LoginController@Adminlogout')->name('admin.logout');
/*//////////////////////////////////////////////////////*/
		Route::get('/state', 'StateController@index')->name('admin.state');
		Route::post('/state/store', 'StateController@store')->name('admin.state.store');
		Route::get('/state/{id}', 'StateController@index')->name('admin.state.edit');
		Route::get('/state/update/{id}', 'StateController@update')->name('admin.state.update');
		Route::delete('/state/destroy/{id}', 'StateController@destroy')->name('admin.state.destroy');

		Route::get('/city', 'CityController@index')->name('admin.city');
		Route::post('/city/store', 'CityController@store')->name('admin.city.store');
		Route::get('/city/{id}', 'CityController@index')->name('admin.city.edit');
		Route::get('/city/update/{id}', 'CityController@update')->name('admin.city.update');
		Route::delete('/city/destroy/{id}', 'CityController@destroy')->name('admin.city.destroy');
		/*////////////////////////////////////////////*/
		Route::get('/categorie', 'CategorieController@index')->name('admin.categorie');
		Route::post('/categorie/store', 'CategorieController@store')->name('admin.categorie.store');
		Route::get('/categorie/{id}', 'CategorieController@index')->name('admin.categorie.edit');
		Route::get('/categorie/update/{id}', 'CategorieController@update')->name('admin.categorie.update');
		Route::delete('/categorie/destroy/{id}', 'CategorieController@destroy')->name('admin.categorie.destroy');
		/*////////////////////////////////////////////*/
		Route::get('/channel', 'ChannelController@index')->name('admin.channel');
		Route::post('/channel/store', 'ChannelController@store')->name('admin.channel.store');
		Route::get('/channel/{id}', 'ChannelController@index')->name('admin.channel.edit');
		Route::get('/channel/update/{id}', 'ChannelController@update')->name('admin.channel.update');
		Route::delete('/channel/destroy/{id}', 'ChannelController@destroy')->name('admin.channel.destroy');
		/*////////////////////////////////////////////*/
		Route::get('/package', 'PackageController@index')->name('admin.package');
		Route::post('/package/store', 'PackageController@store')->name('admin.package.store');
		Route::get('/package/{id}', 'PackageController@index')->name('admin.package.edit');
		Route::get('/package/update/{id}', 'PackageController@update')->name('admin.package.update');
		Route::delete('/package/destroy/{id}', 'PackageController@destroy')->name('admin.package.destroy');


/*//////////////////////////////////////////////////////*/
    Route::prefix('mso')->group(function () {
	    Route::get('/', 'MsoController@index')->name('admin.mso');
		    Route::get('/create', 'MsoController@create')->name('admin.mso.create');
		    Route::post('/store', 'MsoController@store')->name('admin.mso.store');
	    Route::get('/{id}', 'MsoController@show')->name('admin.mso.show');
		    Route::get('/{id}/edit', 'MsoController@edit')->name('admin.mso.edit');
		    Route::get('/update/{id}', 'MsoController@update')->name('admin.mso.update');

	    Route::delete('/destroy/{id}', 'MsoController@destroy')->name('admin.mso.destroy');
	});
	Route::prefix('lco')->group(function () {
	    Route::get('/', 'AdminlcoController@index')->name('admin.lco');
		    Route::get('/create', 'AdminlcoController@create')->name('admin.lco.create');
		    Route::post('/store', 'AdminlcoController@store')->name('admin.lco.store');
	    Route::get('/{id}', 'AdminlcoController@show')->name('admin.lco.show');
		    Route::get('/{id}/edit', 'AdminlcoController@edit')->name('admin.lco.edit');
		    Route::get('/update/{id}', 'AdminlcoController@update')->name('admin.lco.update');

	    Route::delete('/destroy/{id}', 'AdminlcoController@destroy')->name('admin.lco.destroy');
	});
    
    
});

Route::prefix('lco')->group(function () {
	Route::get('/', 'LcoController@index')->name('lco');
	Route::post('/logout', 'Auth\LoginController@Lcologout')->name('lco.logout');
	Route::get('/profile', 'LcoController@profile')->name('lco.profile');
	  Route::get('edit', 'LcoController@edit')->name('lco.edit');
	  Route::get('/update', 'LcoController@update')->name('lco.update');
});

//  Image Uploads
/*Route::post('/api/uploads', 'ImageUploadsController@upload')->name('images.store');*/